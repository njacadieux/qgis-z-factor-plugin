# QGIS Z Factor plugin

Will return the z factor and the scale of a raster in meters. Useful when making a hillshade model when the x, y is in degrees but the z is in meters (ex: SRTM).